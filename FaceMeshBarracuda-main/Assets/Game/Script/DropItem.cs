using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    public float speed = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
    }

    private void Update()
    {
        if (transform.position.y >= -1)
        {
            var pos = transform.position;
            pos.y -= Time.deltaTime * speed;
            transform.position = pos;
        }
        else
        {
            Destroy(gameObject);
        }
    }

}
