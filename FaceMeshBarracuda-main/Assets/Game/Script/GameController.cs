using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject[] Face;
    public float respawnTime;
    private Vector3 screenBounds;
    [HideInInspector] public FaceDirection.Direction direction;
    [HideInInspector] public FaceDirection.State state;
    [HideInInspector] public int score = 0;
    // Start is called before the first frame update
    void Start()
    {
        screenBounds = new Vector3(-0.75f, 0.6f, 98);
        StartCoroutine(DropWave());
        direction = FaceDirection.Direction.NULL;
    }

    private void spawnItem()
    {
        var rand = Random.Range(0, 2);
        GameObject a = Instantiate(Face[rand]);
        if (rand == 0 && direction == FaceDirection.Direction.NULL)
        {
            direction = FaceDirection.Direction.Left;
        }
        else if (rand == 1 && direction == FaceDirection.Direction.NULL)
        {
            direction = FaceDirection.Direction.Right;
        }
        a.transform.position = new Vector3(Random.Range(0, screenBounds.x), screenBounds.y * 4, screenBounds.z);
    }

    IEnumerator DropWave()
    {
        while(true)
        {
            yield return new WaitForSeconds(respawnTime);
            spawnItem();
        }
    }

    public void AddScore()
    {
        if ((state == FaceDirection.State.Left && direction == FaceDirection.Direction.Left) || (state == FaceDirection.State.Right && direction == FaceDirection.Direction.Right))
        {
            direction = FaceDirection.Direction.NULL;
            score += 1;
            Debug.Log(score);
        }
        
    }
}
