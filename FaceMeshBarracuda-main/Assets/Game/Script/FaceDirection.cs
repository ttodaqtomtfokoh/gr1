using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceDirection
{
    // Start is called before the first frame update
    public enum Direction
    {
        Left, Right, NULL
    }

    public enum State
    {
        Left, Right, Straight
    }
}
